﻿using UnityEngine;

public class BallController : MonoBehaviour
{
    public float Speed = .4f;

    void Update()
    {
        transform.position += Vector3.down * Speed * Time.deltaTime;
        ;
    }
}
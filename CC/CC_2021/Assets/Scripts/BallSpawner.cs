﻿using UnityEngine;

public class BallSpawner : MonoBehaviour
{
    public GameObject BallPrefab;

    private float timeout;
    private float lastSpawnTime;

    private void Start()
    {
        timeout = Random.Range(2f, 10f);
        lastSpawnTime = Time.time;
    }

    void Update()
    {
        if (Time.time - lastSpawnTime > timeout)
        {
            Instantiate(BallPrefab, transform.position, transform.rotation);
            timeout = Random.Range(2f, 10f);
            lastSpawnTime = Time.time;
        }
    }
}
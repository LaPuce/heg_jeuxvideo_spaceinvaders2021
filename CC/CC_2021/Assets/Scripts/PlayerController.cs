﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public void Move(float _pos)
    {
        transform.position = new Vector3(_pos, transform.position.y, transform.position.z);
    }
}
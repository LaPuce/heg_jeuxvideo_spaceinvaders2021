﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallDetector : MonoBehaviour
{
    public GameObject GameOver;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameOver.SetActive(true);
    }
}

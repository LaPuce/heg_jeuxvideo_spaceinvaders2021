﻿using System;

public enum InputTypes {
    Left,
    Right,
    Up,
    Down,
    Fire,
}

public class InputEventArgs : EventArgs
{
    public InputTypes InputType;

    public InputEventArgs() { 
    }

    public InputEventArgs(InputTypes _inputType) {
        InputType = _inputType;
    }
}
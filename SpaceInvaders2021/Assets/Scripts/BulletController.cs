﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BulletController : MonoBehaviour
{
    public float Speed = .1f;
    public bool IsEnnemy;

    private BulletPooler bulletPooler;

    void Update()
    {
        if (IsEnnemy)
        transform.position = new Vector3(transform.position.x, transform.position.y - Speed * Time.deltaTime, transform.position.z);
        else
            transform.position = new Vector3(transform.position.x, transform.position.y + Speed * Time.deltaTime, transform.position.z);
    }

    public void SetBulletPooler(BulletPooler _bulletPooler)
    {
        bulletPooler = _bulletPooler;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Wall_Top") {
            // désactiver bullet
            bulletPooler.DeactivateBullet(this.gameObject);
        }
        if (other.name == "Player") {
            SceneManager.LoadScene("Menu");
        }
    }
}
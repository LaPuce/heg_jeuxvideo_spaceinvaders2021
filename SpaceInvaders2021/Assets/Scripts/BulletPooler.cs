﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPooler : MonoBehaviour
{
    public GameObject PlayerBulletPrefab, EnnemyBulletPrefab;

    // taille max de la pool de bullets
    public int PoolSize = 20;

    // liste d'instances de bullets
    private List<GameObject> PlayerBullets;
    private List<GameObject> EnnemyBullets;

    private void Start()
    {
        AbstractShootNotifyer.OnShootEvent += AbstractShootNotifyer_OnShootEvent;

        GameObject playerBulletInstance = null;
        GameObject ennemyBulletInstance = null;
        PlayerBullets = new List<GameObject>();
        EnnemyBullets = new List<GameObject>();
        for (int i = 0; i < PoolSize; i++)
        {
            playerBulletInstance = Instantiate(PlayerBulletPrefab, transform);
            ennemyBulletInstance = Instantiate(EnnemyBulletPrefab, transform);

            // give the bullet a ref to the pooler (for deactivation)
            playerBulletInstance.GetComponent<BulletController>().SetBulletPooler(this);
            ennemyBulletInstance.GetComponent<BulletController>().SetBulletPooler(this);

            // désactiver tous les bullets dans la liste
            playerBulletInstance.SetActive(false);
            PlayerBullets.Add(playerBulletInstance);
            ennemyBulletInstance.SetActive(false);
            EnnemyBullets.Add(ennemyBulletInstance);
        }
    }

    public void OnDisable(){
        AbstractShootNotifyer.OnShootEvent -= AbstractShootNotifyer_OnShootEvent;
    }

    private void AbstractShootNotifyer_OnShootEvent(ShooterEventArgs e)
    {
        if (e.IsEnnemy)
        {
            ActivateNextBullet(EnnemyBullets, e.Position);
        }
        else
        {
            ActivateNextBullet(PlayerBullets, e.Position);
        }
    }

    // Active le prochain bullet disponible dans le pool
    public void ActivateNextBullet(List<GameObject> _bullets, Transform _position) {
        for (int i = 0; i < PoolSize; i++)
        {
            // si le gameobject n'est pas actif dans la hièrarchie
            if (!_bullets[i].activeInHierarchy)
            {
                // active-le
                _bullets[i].SetActive(true);

                // place-le au même endroit que le joueur
                _bullets[i].transform.position = _position.position;

                // stop la méthode
                return;
            }
        }
    }

    // deactivate bullet when touching 'top'
    public void DeactivateBullet(GameObject _bullet)
    {
        _bullet.SetActive(false);
    }
}
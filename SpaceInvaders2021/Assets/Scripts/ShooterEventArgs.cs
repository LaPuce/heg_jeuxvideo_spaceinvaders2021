﻿using System;
using UnityEngine;

public class ShooterEventArgs : EventArgs
{
    public object Sender;
    public Transform Position;
    public bool IsEnnemy;

    public ShooterEventArgs(object _sender)
    {
        Sender = _sender;
    }

    public ShooterEventArgs(object _sender, Transform _position, bool _isEnnemy) {
        Sender = _sender;
        Position = _position;
        IsEnnemy = _isEnnemy;
    }
}
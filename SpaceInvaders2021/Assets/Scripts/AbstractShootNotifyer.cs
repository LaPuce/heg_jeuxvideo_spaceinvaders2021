﻿using UnityEngine;

public abstract class AbstractShootNotifyer : MonoBehaviour
{
    public delegate void ShootEventHandler(ShooterEventArgs e);
    public static event ShootEventHandler OnShootEvent;

    private ShooterEventArgs eventArgs;

    private void Awake()
    {
        eventArgs = new ShooterEventArgs(this);
    }

    protected void AskBulletNotification(Transform _pos, bool _isEnnemy) {
        eventArgs.Position = _pos;
        eventArgs.IsEnnemy = _isEnnemy;
        OnShootEvent?.Invoke(eventArgs);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetTexture : MonoBehaviour
{
    public Material material;
    public float Speed = 1f;

    void Update()
    {
        float offset = Time.time * Speed;
        material.SetTextureOffset("_MainTex", new Vector2(0, offset));
    }
}

﻿using System.Collections;
using UnityEngine;

public class EnnemyController : AbstractShootNotifyer
{

    public float Speed = 1f;

    private int direction = 1;

    private void Start()
    {
        StartCoroutine(Shoot());
    }

    void Update()
    {
        transform.Translate(Vector3.right * Speed * Time.deltaTime * direction);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Wall_Left" || other.name == "Wall_Right")
            direction *= -1;
    }

    public IEnumerator Shoot()
    {
        // demander au pooler un projectile
        AskBulletNotification(transform, true);

        // attendre un temps aléatoire
        yield return new WaitForSeconds(Random.Range(1, 2));

        // after yield
        StartCoroutine(Shoot());
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioShoot : MonoBehaviour
{
    private AudioSource audio;

    public void Start() {
        InputManager.OnInputEvent += InputManager_OnInputEvent;

        audio = GetComponent<AudioSource>();
    }

    private void InputManager_OnInputEvent(object sender, InputEventArgs e)
    {
        if (e.InputType.Equals(InputTypes.Fire))
        {
            // TODO 1: play shoot audio
            audio.Play();
        }
    }
}

﻿using System;

public class BulletEventArgs : EventArgs
{
    public object Sender;
    public String Nom;

    public BulletEventArgs(object _sender, String _nom) {
        Sender = _sender;
        Nom = _nom;
    }
}
﻿using System;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static event EventHandler<InputEventArgs> OnInputEvent;

    private InputEventArgs inputEventArgs;

    private void Start()
    {
        inputEventArgs = new InputEventArgs();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            inputEventArgs.InputType = InputTypes.Left;
            OnInputEvent?.Invoke(this, inputEventArgs);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            inputEventArgs.InputType = InputTypes.Right;
            OnInputEvent?.Invoke(this, inputEventArgs);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            inputEventArgs.InputType = InputTypes.Fire;
            OnInputEvent?.Invoke(this, inputEventArgs);
        }
    }
}
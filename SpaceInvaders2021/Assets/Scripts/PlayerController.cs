﻿using UnityEngine;

public class PlayerController : AbstractShootNotifyer
{
    public float Speed = .1f;
    public bool touchLeft, touchRight;
    public GameObject BulletPrefab;

    private void Start()
    {
        InputManager.OnInputEvent += InputManager_OnInputEvent;
        touchLeft = false;
        touchRight = false;
    }

    public void OnDisable(){
        InputManager.OnInputEvent -= InputManager_OnInputEvent;
    }

    private void InputManager_OnInputEvent(object sender, InputEventArgs e)
    {
        if (e.InputType == InputTypes.Fire)
            AskBulletNotification(transform, false);
        else
            Move(e);
    }

    void Move(InputEventArgs e)
    {
        if (e.InputType == InputTypes.Left && !touchLeft)
            transform.position = new Vector3(transform.position.x - Speed * Time.deltaTime, transform.position.y, transform.position.z);

        if (e.InputType == InputTypes.Right && !touchRight)
            transform.position = new Vector3(transform.position.x + Speed * Time.deltaTime, transform.position.y, transform.position.z);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Wall_Left")
            touchLeft = true;
        else if (other.name == "Wall_Right")
            touchRight = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "Wall_Left")
            touchLeft = false;
        else if (other.name == "Wall_Right")
            touchRight = false;
    }
}
